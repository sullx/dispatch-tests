#include <iostream>
#include "unpacker_factory.hpp"

int main()
{
    auto which = [](UnpackerBase* unpacker) {
        if (auto unpacker1 = dynamic_cast<Unpacker1*>(unpacker))
        {
            std::cout << "Unpacker1 exists at " << std::hex << unpacker1 << std::endl;
        }
        else if (auto unpacker2 = dynamic_cast<Unpacker2*>(unpacker))
        {
            std::cout << "Unpacker2 exists at " << std::hex << unpacker2 << std::endl;
        }
        else
        {
            std::cout << "No unpacker exists at " << std::hex << unpacker2 << std::endl;
        }
    };

    auto unpacker_a = get_unpacker(0x10000002);
    auto unpacker_b = get_unpacker(0x10000001);
    auto unpacker_c = get_unpacker(0x10000003);
    which(unpacker_a.get());
    which(unpacker_b.get());
    which(unpacker_c.get());

    return 0;
}
