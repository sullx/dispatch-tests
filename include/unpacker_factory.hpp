#pragma once
#include <unordered_map>
#include <memory>
#include <functional>
#include "unpackers.hpp"

std::unique_ptr<UnpackerBase> get_unpacker(unsigned int id)
{
    static std::unordered_map<unsigned int, std::function<std::unique_ptr<UnpackerBase>()> > dispatcher
    {
        #define REGISTER(unpacker) {unpacker::global_id, unpacker::construct},
        #include "unpacker_tbl.hpp"
        #undef REGISTER
    };

    std::unique_ptr<UnpackerBase> result = nullptr;
    if (dispatcher.count(id))
    {
        result = std::move(dispatcher[id]());
    }
    return result;
}
