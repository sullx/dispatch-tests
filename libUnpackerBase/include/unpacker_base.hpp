#pragma once

class UnpackerBase
{
public:
    UnpackerBase() = default;
    virtual ~UnpackerBase() = default;
    virtual void read_event(unsigned char* data) = 0;
};
