#include "unpacker2.hpp"

std::unique_ptr<UnpackerBase> Unpacker2::construct()
{
    return std::unique_ptr<UnpackerBase>(new Unpacker2);
}

void Unpacker2::read_event(unsigned char* data)
{
    unpacked_item = *reinterpret_cast<int*>(data);
}
