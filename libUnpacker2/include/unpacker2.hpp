#pragma once
#include <memory>
#include "unpacker_base.hpp"

class Unpacker2 : public UnpackerBase
{
public:
    Unpacker2() = default;
    virtual ~Unpacker2() = default;
    virtual void read_event(unsigned char* data);

    static std::unique_ptr<UnpackerBase> construct();
    static const unsigned int global_id = 0x10000002;
private:
    int unpacked_item = 0;
};
