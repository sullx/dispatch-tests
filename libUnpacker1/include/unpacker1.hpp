#pragma once
#include <memory>
#include "unpacker_base.hpp"

class Unpacker1 : public UnpackerBase
{
public:
    Unpacker1() = default;
    virtual ~Unpacker1() = default;
    virtual void read_event(unsigned char* data);

    static std::unique_ptr<UnpackerBase> construct();
    static const unsigned int global_id = 0x10000001;
private:
    float unpacked_item = 0.0f;
};
