#include "unpacker1.hpp"

std::unique_ptr<UnpackerBase> Unpacker1::construct()
{
    return std::unique_ptr<UnpackerBase>(new Unpacker1);
}

void Unpacker1::read_event(unsigned char* data)
{
    auto val = *reinterpret_cast<float*>(data);
    unpacked_item = 1.0f/val;
}
